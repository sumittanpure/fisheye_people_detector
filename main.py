from PIL import Image
from utils.api import Detector
from utils.visualization import draw_dt_on_np
import cv2
import numpy as np
from datetime import datetime


frame_counter = 1
skip_frame = 20
# Initialize detector
detector = Detector(model_name='rapid',
                    weights_path='/home/sumittanpure/test_codes/RAPiD/weights/pL1_MWHB1024_Mar11_4000.ckpt')




cap = cv2.VideoCapture('/home/sumittanpure/test_codes/RAPiD/video/Fisheye camera video.mp4')
while(cap.isOpened()):
    ret1, frame = cap.read() 
    before_time = datetime.now()
    if frame is not None:
        frame_counter+= 1
        if frame_counter % skip_frame == 0:
            """
            Converting CV2 image to PIL format 
            """
            pil_frame = Image.fromarray(frame) 

            # Getting Bounding boxes in Tensor format 
            boxes = detector._predict_pil(pil_frame, input_size=1024,conf_thres=0.3)
            
            """
            Converting PIL image to CV@ format
            """
            img = np.array(pil_frame)

            # Extracting the main frame with bounding boxes drawn all over 
            im = draw_dt_on_np(img, boxes, color=(0,0,255))

            cv2.imwrite('im.jpg', im)
            after_time = datetime.now() - before_time
            print(after_time)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        print("Frame NONE")
        break