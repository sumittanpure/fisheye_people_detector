import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from tqdm import tqdm

import torch
import torchvision.transforms.functional as tvf

from utils import visualization, utils


class Detector():
    '''
    Wrapper of image object detectors.

    Args:
        model_name: str, currently only support 'rapid'
        weights_path: str, path to the pre-trained network weights
        model: torch.nn.Module, used only during training
        conf_thres: float, confidence threshold
        input_size: int, input resolution
    '''
    def __init__(self, model_name='', weights_path=None, model=None, **kwargs):
        # post-processing settings
        self.conf_thres = kwargs.get('conf_thres', None)
        self.input_size = kwargs.get('input_size', None)

        if model:
            self.model = model
            return
        if model_name == 'rapid':
            from models.rapid import RAPiD
            model = RAPiD(backbone='dark53')
        elif model_name == 'rapid_export': # testing-only version
            from models.rapid_export import RAPiD
            model = RAPiD()
        else:
            raise NotImplementedError()
        total_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
        print(f'Successfully initialized model {model_name}.',
            'Total number of trainable parameters:', total_params)
        
        model.load_state_dict(torch.load(weights_path, map_location=torch.device('cpu'))['model'])
        print(f'Successfully loaded weights: {weights_path}')
        model.eval()
        #UNCOMMENT TO USE CUDA
        # if kwargs.get('use_cuda', True):
        #     print("Using CUDA...")
        #     assert torch.cuda.is_available()
        #     self.model = model.cuda()
        # else:
        print("Using CPU instead of CUDA...")
        self.model = model

    
    def _predict_pil(self, pil_img, **kwargs):
        '''
        Args:
            pil_img: PIL.Image.Image
            input_size: int, input resolution
            conf_thres: float, confidence threshold
        '''
        input_size = kwargs.get('input_size', self.input_size)
        conf_thres = kwargs.get('conf_thres', self.conf_thres)
        assert isinstance(pil_img, Image.Image), 'input must be a PIL.Image'
        assert input_size is not None, 'Please specify the input resolution'
        assert conf_thres is not None, 'Please specify the confidence threshold'

        # pad to square
        input_img, _, pad_info = utils.rect_to_square(pil_img, None, input_size, 0)

        input_ori = tvf.to_tensor(input_img)
        input_ = input_ori.unsqueeze(0)

        assert input_.dim() == 4
        device = next(self.model.parameters()).device
        input_ = input_.to(device=device)
        with torch.no_grad():
            dts = self.model(input_).cpu()

        dts = dts.squeeze()
        # post-processing
        dts = dts[dts[:,5] >= conf_thres]
        if len(dts) > 1000:
            _, idx = torch.topk(dts[:,5], k=1000)
            dts = dts[idx, :]
        
        dts = utils.nms(dts, is_degree=True, nms_thres=0.45, img_size=input_size)
        dts = utils.detection2original(dts, pad_info.squeeze())
        
        return dts

